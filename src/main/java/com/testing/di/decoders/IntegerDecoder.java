package com.testing.di.decoders;

import java.text.NumberFormat;
import java.text.ParseException;

import org.milyn.javabean.DataDecodeException;
import org.milyn.javabean.DecodeType;
import org.milyn.javabean.decoders.NumberDecoder;

@DecodeType({ Integer.class, int.class })
public class IntegerDecoder extends NumberDecoder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Object decode(String data) throws DataDecodeException {
		NumberFormat format = getNumberFormat();

		if (format != null) {
			try {
				Number number = format.parse(data.trim());

				if (isPercentage()) {
					return (int) (number.doubleValue() * 100);
				} else {
					return number.intValue();
				}
			} catch (ParseException e) {
				throw new DataDecodeException(
						"Failed to decode Integer value '" + data + "' using NumberFormat instance " + format + ".", e);
			}
		} else {
			try {
				if (data.trim().length() > 0) {
					return Integer.parseInt(data.trim());
				} else {
					return null;
				}
			} catch (NumberFormatException e) {
				throw new DataDecodeException("Failed to decode Integer value '" + data + "'.", e);
			}
		}
	}
}
