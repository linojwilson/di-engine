package com.testing.di.decoders;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.text.ParseException;

import org.milyn.javabean.DataDecodeException;
import org.milyn.javabean.DecodeType;
import org.milyn.javabean.decoders.NumberDecoder;

@DecodeType(BigDecimal.class)
public class BigDecimalDecoder extends NumberDecoder {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Object decode(String data) throws DataDecodeException {
        NumberFormat format = getNumberFormat();

        if(format != null) {
            try {
                Number number = format.parse(data.trim());

                if(number instanceof BigDecimal) {
                    return number;
                } else if(number instanceof BigInteger) {
                    return new BigDecimal((BigInteger) number);
                }

                return new BigDecimal(number.doubleValue());
            } catch (ParseException e) {
                throw new DataDecodeException("Failed to decode BigDecimal value '" + data + "' using NumberFormat instance " + format + ".", e);
            }
        } else {
            try {
            	if(data.trim().length()>0) {
            		return new BigDecimal(data.trim());	
            	}else {
            		return null;
            	}
                
            } catch(NumberFormatException e) {
                throw new DataDecodeException("Failed to decode BigDecimal value '" + data + "'.", e);
            }
        }
    }
}