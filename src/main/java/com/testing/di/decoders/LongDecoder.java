package com.testing.di.decoders;

import java.text.NumberFormat;
import java.text.ParseException;

import org.milyn.javabean.DataDecodeException;
import org.milyn.javabean.DecodeType;
import org.milyn.javabean.decoders.NumberDecoder;

@DecodeType({ Long.class, long.class })
public class LongDecoder extends NumberDecoder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Object decode(String data) throws DataDecodeException {
		NumberFormat format = getNumberFormat();

		if (format != null) {
			try {
				Number number = format.parse(data.trim());

				if (isPercentage()) {
					return (long) (number.doubleValue() * 100);
				} else {
					return number.longValue();
				}
			} catch (ParseException e) {
				throw new DataDecodeException(
						"Failed to decode Long value '" + data + "' using NumberFormat instance " + format + ".", e);
			}
		} else {
			try {
				if(data.trim().length()>0) {
					return Long.parseLong(data.trim());	
				}else {
					return null;
				}
				
			} catch (NumberFormatException e) {
				throw new DataDecodeException("Failed to decode Long value '" + data + "'.", e);
			}
		}
	}
}
