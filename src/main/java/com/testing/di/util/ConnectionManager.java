/**
 * 
 */
package com.testing.di.util;

import java.sql.Connection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author linoj.wilson
 * 
 */
public class ConnectionManager {

	private static Logger LOGGER = LoggerFactory
			.getLogger(ConnectionManager.class);

	protected static AbstractApplicationContext context;
	protected static DataSource dataSource;
	protected static boolean loaded = false;
	protected static EntityManagerFactory entityManager;
	private static ConnectionManager instance = new ConnectionManager();
	
	private ConnectionManager() {
		
	}

	/**
	 * 
	 */
	static {

		try {
			context = new ClassPathXmlApplicationContext("di-db.xml");
			dataSource = context.getBean(DataSource.class);
			entityManager = context.getBean(EntityManagerFactory.class);
			loaded = true;
		}

		catch (Throwable ex) {
			LOGGER.warn("di-db.xml not configured, ignoring. " +
					"Probably, don't wanna use JPA");
			loaded = false;
			
		}

	}

	public Connection getConnection() throws Throwable {
		return dataSource.getConnection();// FIXME - why auto commit to false
											// missing?
	}

	public EntityManager getJpaConnection() throws Throwable {
		return entityManager.createEntityManager();
	}

	public void cleanup() {
		LOGGER.info("Shutting down db pool, please wait");
		if (context != null)
			context.close();
	}

	/**
	 * @return the instance
	 */
	public static ConnectionManager getInstance() {
		return instance;
	}

	/**
	 * @return the loaded
	 */
	public static boolean isLoaded() {
		return loaded;
	}

}
