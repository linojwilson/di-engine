/**
 * 
 */
package com.testing.di.util;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author linoj.wilson
 *
 */
public class HaltJvm {
	
	/**
	 * halt jvm.
	 * 
	 * @param status
	 * @param maxDelayMillis
	 */
	public static void exit(final int status, long maxDelayMillis) {
	    try {
	      // setup a timer, so if nice exit fails, the nasty exit happens
	      Timer timer = new Timer();
	      timer.schedule(new TimerTask() {
	        @Override
	        public void run() {
	          Runtime.getRuntime().halt(status);
	        }
	      }, maxDelayMillis);
	      // try to exit nicely
	      System.exit(status);
	      
	    } catch (Throwable ex) {
	      // exit nastily if we have a problem
	      Runtime.getRuntime().halt(status);
	    } finally {
	      // should never get here
	      Runtime.getRuntime().halt(status);
	    }
	  }

}
