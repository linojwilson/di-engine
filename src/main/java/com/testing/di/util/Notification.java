package com.testing.di.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author linoj.wilson
 *
 */
public class Notification 

{
	private String msgType;
	private String category;
	private String action;
	private String msg;
	private Long solutionId;
	private String user;
	private String status;
	private String fromAddress;
	private String toAddress;
	private String firstName;
	private String lastName;
	
	private Map<String, Object>	payload;
	
	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the payload
	 */
	public Map<String, Object> getPayload() {
		return payload;
	}
	
	public void setMsgHeader(String msgHeader) {
		msg = msgHeader;
	}

	public Notification()  {
		payload  = new HashMap<String, Object>();
		
	}
	
	/**
	 * add a field.
	 * @param key
	 * @param o
	 */
	public void addField(String key, Object o) {
		payload.put(key,o);
	}
	
	/**
	 * adds all fields in one go
	 * @param all
	 */
	public void putAll(Map<String,Object> all) {
		payload.putAll(all);
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @return the solutionId
	 */
	public Long getSolutionId() {
		return solutionId;
	}

	/**
	 * @param solutionId the solutionId to set
	 */
	public void setSolutionId(Long solutionId) {
		this.solutionId = solutionId;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the fromAddress
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @param fromAddress the fromAddress to set
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	/**
	 * @return the toAddress
	 */
	public String getToAddress() {
		return toAddress;
	}

	/**
	 * @param toAddress the toAddress to set
	 */
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @param payload the payload to set
	 */
	public void setPayload(Map<String, Object> payload) {
		this.payload = payload;
	}
	
}
