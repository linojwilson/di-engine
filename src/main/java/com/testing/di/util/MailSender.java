/**
 * 
 */
package com.testing.di.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author linoj.wilson
 *
 */
public class MailSender {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MailSender.class);

	public Properties init() throws Throwable {
		
		Properties config = null;

		try

		{
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("mail-sender.properties");
			config = new Properties();
			config.load(is);
			

		}

		finally
		
		{
			//do nothing.
		}
		
		return config;

	}

	/**
	 * @param args
	 * @throws Throwable 
	 */
	public static void main(String[] args) throws Throwable {
		if ((args == null) || (args.length != 1)) {
			throw new IllegalArgumentException();
		}
		(new MailSender()).send(args[0]);

	}

	private void send(String msg) throws Throwable

	{

		Properties config = init();
		NotificationHandler handler = null;
		boolean notify = Boolean.parseBoolean(config.getProperty("engine.notification.send"));
		if (notify)

		{
			try {
				handler = (NotificationHandler) Class.forName(config.getProperty("engine.notification.handler"))
						.newInstance();
				Map<String, Object> msgFields = parse(msg);
				handler.notify(build(msgFields,config));
				
			} catch (Throwable t)

			{
				LOGGER.error(t.getMessage(), t);
			}
		}

	}

	

	/**
	 * send out notification to event framework
	 * 
	 * @param msgFields
	 */
	private Notification build(Map<String, Object> msgFields,
			Properties config) {
		Notification n = new Notification();
		n.putAll(msgFields);

		// first fetch all mandatory fields
		// in the message.
		buildHeader(n, msgFields);

		// add recipient
		n.setFirstName(config.getProperty("engine.notification.first-name"));
		n.setLastName(config.getProperty("engine.notificaiton.last-name"));
		n.setToAddress(config.getProperty("engine.notification.to-address"));
		
		return n;

	}

	/**
	 * look for mandatory messages and prepare notification header.
	 * 
	 * @param n
	 * @param msgFields
	 */
	private void buildHeader(Notification n, Map<String, Object> msgFields) {

		// do we have category?
		Object o = msgFields.get("category");
		if (o == null) {
			throw new IllegalArgumentException("event category missing");
		}
		n.setCategory(((String) o).trim().toLowerCase());

		// do we have solution id?
		o = msgFields.get("solution-id");
		if (o == null) {
			throw new IllegalArgumentException("solution-id missing");
		}
		n.setSolutionId(Long.parseLong((String) o));

		// do we have message type?
		o = msgFields.get("msg-type");
		if (o == null) {
			throw new IllegalArgumentException("msg-type missing");
		}
		n.setMsgType(((String) o).trim().toLowerCase());

		// do we have action?
		o = msgFields.get("action");
		if (o == null) {
			throw new IllegalArgumentException("action missing");
		}
		n.setAction(((String) o).trim().toLowerCase());
		
		// setup message header.
		o = msgFields.get("msg-header");
		if (o == null) {
			o = "No subject";
		}
		n.setMsgHeader((String)o);
		
		// setup message header.
		o = msgFields.get("user-id");
		if (o == null) {
			o = "di-engine";
		}
		n.setUser((String)o);
		n.setStatus("bye!");
		
	}

	/**
	 * parse input message
	 * 
	 * @param msg
	 * @return
	 */
	private Map<String, Object> parse(String msg) {

		// parse input parameters and prepare the input
		// message for default notify handler.
		Map<String, Object> msgTokens = new HashMap<String, Object>();
		String[] msgFields = msg.split(",");
		for (String msgField : msgFields)

		{
			String[] keyValue = msgField.split("=");
			if (keyValue.length != 2) {
				throw new IllegalArgumentException("<KEY>=<VALUE> supported.");
			}
			msgTokens.put(keyValue[0], keyValue[1]);
		}

		return msgTokens;
	}

}
