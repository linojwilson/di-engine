/**
 * 
 */
package com.testing.di.util;

/**
 * @author linoj.wilson
 *
 */
public interface NotificationHandler {
	
	/**
	 * send notifications
	 * @param notification
	 */
	public void notify(Notification notification);

}
