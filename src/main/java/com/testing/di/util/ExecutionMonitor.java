package com.testing.di.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author linoj.wilson
 *
 */
public class ExecutionMonitor {
	
	/*
	 * setup counters
	 */
	private Integer msgTotal = 0;
	private Integer msgValidCount = 0;
	private Integer msgInvalidCount = 0;
	private Integer totalRecords = 0;
	private Integer rejectedRecordCount = 0;
	private Integer failedRecordCount = 0;
	private Integer validRecordCount = 0;
	private Map<String,Object> monitoredValues ;
	
	public ExecutionMonitor() {
		monitoredValues =  new HashMap<String,Object>();
	}
	
	public void reset() 
	
	{
		msgTotal = 0;
		msgValidCount = 0;
		msgInvalidCount = 0;
		totalRecords = 0;
		rejectedRecordCount = 0;
		failedRecordCount = 0;
		validRecordCount = 0;
	}
	
	
	/**
	 * increment processed messages
	 */
	public void incrMsgTotal() {
		msgTotal++;
	}

	/**
	 * increment valid message count
	 */
	public void incrMsgValidCount() {
		msgValidCount++;
	}
	
	/**
	 * increment invalid message count.
	 */
	public void incrMsgInvalidCount() {
		msgInvalidCount++ ;
	}
	
	
	/**
	 * increments total record count.
	 */
	public void incrTotalRecords() {
		totalRecords++;
	}
	
	/**
	 * increment rejected record count
	 */
	public void incrRejectedRecordCount() {
		rejectedRecordCount++;
	}
	
	/**
	 * increment failed record count
	 */
	public void incrFailedRecordCount() {
		failedRecordCount++;
	}
	
	/**
	 * increment failed record count
	 */
	public void incrValidRecordCount() {
		validRecordCount++;
	}
	
	/**
	 * get monitored values
	 * 
	 * @return
	 */
	public Map<String,Object> getAll()
	
	{
		monitoredValues.put("msg-total", msgTotal);
		monitoredValues.put("msg-valid-count", msgValidCount);
		monitoredValues.put("msg-invalid-count", msgInvalidCount);
		monitoredValues.put("total-records", totalRecords);
		monitoredValues.put("rejected-records-count", rejectedRecordCount);
		monitoredValues.put("failed-records-count", failedRecordCount);
		monitoredValues.put("valid-records-count", validRecordCount);
				
		return monitoredValues;
	}
	

}
