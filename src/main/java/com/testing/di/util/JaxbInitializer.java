/**
 * 
 */
package com.testing.di.util;


import javax.xml.bind.JAXBContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.testing.di.core.validators.errors.ValidatorExecution;


/**
 * @author linoj.wilson
 *
 */
public class JaxbInitializer {

	private static Logger LOGGER = LoggerFactory.getLogger(JaxbInitializer.class);
	private static JAXBContext jaxbContext;

	static

	{
		try {
			jaxbContext = JAXBContext.newInstance(ValidatorExecution.class);
		} catch (Throwable e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * @return the jaxbContext
	 */
	public static JAXBContext getJaxbContext() {
		return jaxbContext;
	}

}

