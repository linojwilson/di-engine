/**
 * 
 */
package com.testing.di.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
import com.testing.rest.client.EventClient;
import com.testing.rest.client.dto.Destination;
import com.testing.rest.client.dto.Event;
import com.testing.rest.client.dto.Recipient;
import com.testing.rest.client.dto.SnapShot;
*/

/**
 * @author linoj.wilson
 *
 */
public class DefaultNotificationHandler {
	
	private static Logger LOGGER = LoggerFactory
			.getLogger(DefaultNotificationHandler.class);
/*
	@Override
	public void notify(Notification n) {
		
		try
		
		{

			//prepare events as needed.
			Event e = new Event();
			e.setAction(n.getMsgType());
			e.setCategory(n.getCategory());
			e.setEntity(n.getAction());
			e.setSolutionId(n.getSolutionId());
			e.setUserName(n.getUser());
			e.setStatus(n.getStatus());
			e.setCreatedDate(System.currentTimeMillis());
			e.setComment("DI-Engine signal end.");
			
			//setup x-header.
			Map<String,Object> xHeader = new HashMap<String,Object>();
			xHeader.put("subject", n.getMsg());
			//xHeader.put("to-address", n.getToAddress());
			e.setxHeader(xHeader);
			
			Destination channel = new Destination();
			Recipient recipient = new Recipient();
			recipient.setEmailAddress(n.getToAddress());
			recipient.setToEmailAddress(n.getToAddress());
			recipient.setFirstName(n.getFirstName());
			recipient.setLastName(n.getLastName());
			channel.addRecipient(recipient);
			e.setDestination(channel);
			
			//prepare snapshot.
			List<SnapShot> snapShots = new ArrayList<SnapShot>();
			SnapShot s = new SnapShot();
			Map<String,Object> fields = n.getPayload();
			for ( Entry<String,Object> field:
				fields.entrySet()) 
			
			{
				s.addField(field.getKey(), 
						field.getValue());
			}
			
			snapShots.add(s);
			e.setSnapShots(snapShots);
			EventClient el = EventClient.getInstance();
			el.addEvent(e);
			
		}
		
		catch ( Throwable t)
		
		{
			LOGGER.error(t.getMessage(),t);
		}

		
		
	}
*/	
	

}
