package com.testing.di.core.jpa;

import java.io.InputStream;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Converter
public class DateConverter implements AttributeConverter<String, Date> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DateConverter.class);

	private static Properties config;

	static

	{

		try

		{
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("jpa-converter.properties");
			config = new Properties();
			config.load(is);

		}

		catch (Throwable e)

		{
			LOGGER.error(e.getMessage(), e);
		}

	}

	@Override
	public Date convertToDatabaseColumn(String dateAsString) {
		java.sql.Date date = null;
		try {
			if ((dateAsString != null) && dateAsString.trim().length() > 0) {
				SimpleDateFormat format = new SimpleDateFormat(config.getProperty("jpa.date.format"));
				java.util.Date d = format.parse(dateAsString);
				date = new java.sql.Date(d.getTime());
			}

		} catch (Throwable e) {
			LOGGER.error(e.getMessage(), e);
		}
		return date;
	}

	@Override
	public String convertToEntityAttribute(Date date) {
		String d = null;

		try

		{
			if (date != null) {
				DateFormat df = new SimpleDateFormat(config.getProperty("jpa.date.format"));
				d = df.format(date);
			}

		}

		catch (Throwable e)

		{
			LOGGER.error(e.getMessage(), e);
		}

		return d;
	}

}