/**
 * 
 */
package com.testing.di.core.transformers;

import java.util.Collection;
import java.util.Properties;

/**
 * @author linoj.wilson
 *
 */
public interface Transformer {
	
	/**
	 * configure the transformer.
	 * 
	 * @param config
	 */
	public void configure(Properties config);
	
	/**
	 * Transform the contents.
	 * 
	 * @param contents
	 * 
	 * @return
	 * @throws Throwable
	 */
	public Collection<byte[]> transform(byte[] contents) throws Throwable;
	
	

}
