/**
 * 
 */
package com.testing.di.core.transformers;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.milyn.Smooks;
import org.milyn.container.ExecutionContext;

/**
 * @author linoj.wilson
 *
 */
public class ByteTransformer implements Transformer {

	private Properties config;

	@Override
	public void configure(Properties config) {
		this.config = config;

	}

	@Override
	public Collection<byte[]> transform(byte[] contents) throws Throwable {
		String configXml = config.getProperty(config.getProperty("system") + ".message.payload.config");

		Smooks smooks = new Smooks(configXml);

		try {
			ExecutionContext executionContext = smooks.createExecutionContext();
			StringWriter writer = new StringWriter();
			smooks.filterSource(executionContext,
					new StreamSource(new InputStreamReader(new ByteArrayInputStream(contents), "UTF-8")),
					new StreamResult(writer));
			Collection<byte[]> payload = new ArrayList<byte[]>();
			payload.add(writer.toString().getBytes("UTF-8"));
			return payload;
		} finally

		{
			smooks.close();
		}

	}

}
