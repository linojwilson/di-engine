/**
 * 
 */
package com.testing.di.core.readers.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.milyn.io.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.testing.di.core.processors.Message;
import com.testing.di.core.readers.IoSystem;

/**
 * @author linoj.wilson
 *
 */
public class FileSystem implements IoSystem {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FileSystem.class);
	
	
	private List<File> files;
	private int noOfFiles = -1;
	private int currentIndex = 0;

	/* (non-Javadoc)
	 * @see com.testing.bas.core.readers.IoSystem#read(com.testing.bas.core.Message)
	 */
	public byte[] readNext() {
		
		byte [] contents = null;
		if ( ( currentIndex >= 0) && ( currentIndex < noOfFiles)) {
			File f = files.get(currentIndex);
			FileInputStream fs = null;
			try 
			
			{
				fs = new FileInputStream(f);
				contents = StreamUtils.readStream(fs);
			
			} 
			
			catch (Throwable e) 
			
			{
				LOGGER.error(e.getMessage(),e);
				throw new IllegalArgumentException(f
						.getAbsolutePath());
			}
			
			finally
			
			{
				currentIndex++;
				if ( fs != null) {
					try {
						fs.close();
					} catch (IOException ignored) {
						// do nothing.
					}
				}
			}
		}
			
		return contents;
	}

	public void seek(Message msg) {
		seek(msg.getMsgContents());
		
	}

	@Override
	public String getPath() {
		return files.get(currentIndex - 1).getName();
	}

	@Override
	public void seek(String location) {
		files  = new ArrayList<File>();
		final File folder = new File(location);
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isFile()) {
	        	files.add(fileEntry);
	        }
	    }
		
		noOfFiles = files.size();
		if ( noOfFiles > 0) {
			currentIndex = 0;
		}
		
		
	}

}
