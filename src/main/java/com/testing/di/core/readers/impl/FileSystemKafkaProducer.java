/**
 * 
 */
package com.testing.di.core.readers.impl;

import java.io.File;
import java.util.Iterator;
import java.util.Properties;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.testing.di.core.audit.Auditor;
import com.testing.di.core.processors.Message;
import com.testing.di.core.readers.IoSystem;
import com.testing.di.core.readers.kafka.MessageSender;
import com.testing.di.core.readers.kafka.Publisher;
import com.testing.di.core.transformers.Transformer;
import com.testing.di.core.validators.ValidationTypes;
import com.testing.di.util.HaltJvm;
import com.google.common.io.Files;


/**
 * @author linoj.wilson
 *
 */
public class FileSystemKafkaProducer implements Publisher {
	
	private static final Logger log = LoggerFactory.getLogger(FileSystemKafkaProducer.class);

	/* (non-Javadoc)
	 * @see com.testing.bas.core.readers.kafka.Publisher#publish()
	 */
	@Override
	public void publish(Properties config) {
		try
		
		{


			// launch the daemon thread.
			class RunnerThread implements Runnable {
				
				private void mkdirs() {
					
					String system =  config.getProperty("system");
					String sourceType = config.getProperty(system  + 
							".message.sourceType")
							.toLowerCase();
					String basedir = config.getProperty(system + "." 
							+"message.location" + "." + sourceType);
					
					// do we have processed directory?
					String processed = config.getProperty("engine.dir.processed");
					if ( processed == null) processed = "processed";
					File p = new File( basedir + File.separator + processed);
					if (!p.exists()) {
						p.mkdirs();
					}
					
					// do we have processed directory?
					String rejected = config.getProperty("engine.dir.rejected");
					if ( rejected == null) rejected = "processed";
					File r = new File( basedir + File.separator + rejected);
					if (!r.exists()) {
						r.mkdirs();
					}
					
				}
				
				private void move(String filename, String folder) {

					try {
						
						String system = config.getProperty("system");
						String sourceType = config.getProperty(system + ".message.sourceType")
								.toLowerCase();
						String basedir = config.getProperty(system + "." 
								+"message.location" + "." + sourceType);
						String l = basedir + File.separator  + filename;

						File source = new File(l);
						String f = System.currentTimeMillis() + "-" + filename;
						String destination = config.getProperty(system + "." 
								+"message.location" + "." + sourceType) 
								+ File.separator + folder + File.separator
								+ f;
						Files.move(source, new File(destination));
					

					} catch (Exception e) {
						log.error(e.getMessage(),e);
					}

				}

				@Override
				public void run() {

					long dozeOffTime = Long.parseLong(config.getProperty(config.getProperty("system") 
							+ ".message.publisher.scan-interval"));
					dozeOffTime *= (60 * 1000);
					mkdirs();
					
					// see if we have transformer configured.
					String clazz = config.getProperty(config.getProperty("system") 
							+ ".message.payload.transform.clazz");
					Transformer t = null;
					if ( clazz != null) {
						
						try
						
						{
							t = (Transformer) Class.forName(clazz).newInstance();
							t.configure(config);
						}
						
						catch ( Exception e)
						
						{
							log.error(e.getMessage(),e);
							HaltJvm.exit(-1, 1000);
						}
					}
					
					MessageSender sender = null;
					
					while (true) {

						try

						{
							Thread.sleep(dozeOffTime);
							sender = null;//enforce gc.
							// read the configured directory and
							// push the messages to configured
							// topic
							sender = new MessageSender();
							String topic = config.getProperty(config
									.getProperty("system") + ".message.publisher.topic");
							sender.setTopic(topic);
							
							String clientId = config.getProperty(config.getProperty("system") + ".message.publisher.id");
							config.put("producer.id", clientId);
							sender.configure(config);
							
							IoSystem ioSys = new FileSystem();
							ioSys.seek(config.getProperty(config.getProperty("system") 
									+".message.location.file"));
							
							byte[] contents = ioSys.readNext();
							if ( ( contents == null) || ( contents.length == 0))
								continue;
							
							Message msg = new Message();
							while (contents != null) 
							
							{
								
								try
								{
									//send message
									String key = UUID.randomUUID().toString();
									String system = config.getProperty("system");
									msg.setPath(ioSys.getPath());
									msg.setMsgId(key);
									msg.setData(contents);
									
									String sourceType = config.getProperty(	system	+ ".message.sourceType")
											.toLowerCase();
									msg.setSourceType(sourceType);
									msg.setPayloadType(config.getProperty(system +  ".message.format"));
									
									// at this point we don't know the kind of
									// message because it's file. we will decipher
									// at a later stage and update the message
									// type.
									msg.setMsgType(system);
									
									// set message status to "success".
									msg.setStatus(ValidationTypes.SUCCESS.name());
									msg.setDetails(null);
									Auditor.getMessageAuditor().audit(msg);
									
									if ( t != null) {
										// run the transform and dump
										// transformed contents.
										Iterator<byte[]> payload = t.transform(contents).iterator();
										log.info("Key=" + key);
										while ( payload.hasNext()) 
										{
											sender.send(key, payload.next());
										}
										
									}
									
									else {
										// we don't have a transformer. so
										// just dump what we got.
										sender.send(key, contents);
									}
										
									String moveDir = config.getProperty("engine.dir.processed");
									move(ioSys.getPath(),moveDir);
									
								}
								
								catch ( Exception e)
								
								{
									log.error(e.getMessage(),e);
									
									// set message status to "error".
									msg.setStatus(ValidationTypes.PROCESSING_ERROR.name());
									msg.setDetails(e.getMessage());
									Auditor.getMessageAuditor().update(msg);
									String moveDir = config.getProperty("engine.dir.rejected");
									move(ioSys.getPath(),moveDir);
									
								}
								
								finally 
								{
									contents = null;//enforce gc;
									contents = ioSys.readNext();
									
								}
								
								
							}
							

						}

						catch (InterruptedException ex) {
							// can't do much.
							log.warn(ex.getMessage(), ex);
						} catch (Throwable e) {
							log.error(e.getMessage(), e);
						}

						finally {
							if ( sender != null) {
								sender.close();
							}
							
						}
					}

				}

			}

			RunnerThread launcher = new RunnerThread();
			new Thread(launcher).start();

		
		}
		
		catch ( Exception e)
		
		{
			log.error(e.getMessage(),e);
		}

	}

}
