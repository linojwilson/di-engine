/**
 * 
 */
package com.testing.di.core.readers.kafka;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.testing.di.util.ConnectionManager;

/**
 * @author linoj.wilson
 *
 */
public class BasicConsumerService implements ConsumerService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BasicConsumerService.class);

	private String name;
	
	/**
	 * @param args
	 */
	@Override
	public void launch(Properties config) {

		// load configuration and initialize
		// the consumer service.
		String system = config.getProperty("system");
		
		// fetch the number of consumers to launch
		// for the service.
		LOGGER.info("Launching consumer service for " + system);
		int numConsumers = Integer.parseInt(config.getProperty(system +
				".consumer." + name +".number"));
		
		// launch the consumers in a runner thread. A thread will be used
		// for a consumer.
		ExecutorService executor = Executors.newFixedThreadPool(numConsumers);
		final List<MessageConsumer> consumers = new ArrayList<>();
		for (int i = 0; i < numConsumers; i++) {
			MessageConsumer consumer = new MessageConsumer(config, name,
					"id-" + i);
			consumers.add(consumer);
			executor.submit(consumer);
		}

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				
				//shutdown DB pool.
				ConnectionManager.getInstance().cleanup();
				
				//shutdown consumer group.
				for (MessageConsumer consumer : consumers) {
					consumer.shutdown();
				}
				executor.shutdown();
				try {
					executor.awaitTermination(5000, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {
					// ignore.
				}
			}
		});

	}

	@Override
	public void name(String name) {
		this.name = name.trim().toLowerCase();
		
	}

}
