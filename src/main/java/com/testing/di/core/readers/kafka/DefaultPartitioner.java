/**
 * 
 */
package com.testing.di.core.readers.kafka;

import java.util.Map;
import java.util.Random;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

/**
 * @author linoj.wilson
 *
 */
public class DefaultPartitioner implements Partitioner {

	@Override
	public void configure(Map<String, ?> configs) {
		// configuration information for deciding on
		// the partition. we don't have any at this
		// time. add later.
		
	}

	@Override
	public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
		int noOfPartitions = cluster.partitionCountForTopic(topic);
		Random r = new Random();
		int i = r.nextInt(200);
		int partition = i%noOfPartitions;
		System.out.println("partition=" + partition);
        return  partition ;
	}

	@Override
	public void close() {
		// do nothing.
		
	}
   
}