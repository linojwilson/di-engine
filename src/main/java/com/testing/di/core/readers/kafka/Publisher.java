/**
 * 
 */
package com.testing.di.core.readers.kafka;

import java.util.Properties;

/**
 * @author linoj.wilson
 *
 */
public interface Publisher {
	
	public void publish(Properties config);

}
