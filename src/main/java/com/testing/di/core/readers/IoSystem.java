/**
 * 
 */
package com.testing.di.core.readers;

import com.testing.di.core.processors.Message;

/**
 * @author linoj.wilson
 *
 */
public interface IoSystem {
	
	public void seek(Message msg);
	public void seek(String location);
	public byte [] readNext();
	public String getPath();

}
