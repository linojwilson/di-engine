/**
 * 
 */
package com.testing.di.core.readers.kafka;

import java.util.Properties;
import java.util.UUID;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;

/**
 * @author linoj.wilson
 *
 */
public class MessageSender {
	
	private Producer<String, byte[]> producer;
	private String topic;

	/**
	 * setup producer
	 * 
	 * @param config
	 */
	public void configure(Properties config) {
		Properties producerConfig = new Properties();
		producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, config.getProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG));
		producerConfig.put(ProducerConfig.CLIENT_ID_CONFIG, config.getProperty("producer.id"));
		producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getName());
		producerConfig.put(ProducerConfig.ACKS_CONFIG, "all");
		producerConfig.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, config.getProperty(config
				.getProperty("system") + ".message.partitioner"));
		producerConfig.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
		producer =  new KafkaProducer<>(producerConfig);

	}
	
	public void send(byte [] msg) throws Throwable {
		send(UUID.randomUUID().toString(),msg);
	}
	
	public void send(String key, byte [] msg) throws Throwable {
		  try {
	    	  final ProducerRecord<String, byte[]> record =
                    new ProducerRecord<>(topic,key,msg);
	    	 //FIXME we will change to "async" later.
	    	 producer.send(record).get();
           
          
	      } finally {
	          producer.flush();
	         
	      }
		
	}
	
	public void close() {
		 producer.close();
	}

	/**
	 * @param topic the topic to set
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}

}
