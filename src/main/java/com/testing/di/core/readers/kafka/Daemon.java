/**
 * 
 */
package com.testing.di.core.readers.kafka;

import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.testing.di.core.audit.Auditor;
import com.testing.di.util.ConnectionManager;
import com.testing.di.util.HaltJvm;

/**
 * @author linoj.wilson
 *
 */
public class Daemon {

	private static final Logger LOGGER = LoggerFactory.getLogger(Daemon.class);

	/**
	 * entry point
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		(new Daemon()).launch(args);
	}

	protected void launch(String[] args) {
		try

		{
			if ((args == null) || (args.length != 1)) {
				throw new IllegalArgumentException();
			}

			String system = args[0].trim().toLowerCase();
			Server s = new Server(system);
			new Thread(s).start();
		}

		catch (Throwable e)

		{
			LOGGER.error(e.getMessage(), e);
			HaltJvm.exit(-1, 5000);
		}

	}

	private static class Server implements Runnable {

		private String system;

		public Server(String system) {
			this.system = system;
		}

		/**
		 * Load consumer configuration.
		 * 
		 * @param system
		 * @throws Throwable
		 */
		private Properties configure() {

			Properties config = new Properties();
			try

			{
				InputStream is = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream(system + "-di-engine.properties");
				config = new Properties();
				config.load(is);

				Auditor.init(config);

				// setup db Connections. if any errors, let's balk out at
				// this time. don't wait till all consumers are setup,etc.
				ConnectionManager.getInstance();
				config.put("system", system);
				if (ConnectionManager.isLoaded()) {
					config.put("running", "true");
				}

				else {
					config.put("running", "false");

				}

			}

			catch (Throwable e)

			{
				LOGGER.info(e.getMessage(), e);
				config.put("running", "false");
			}

			return config;

		}

		@Override
		public void run() {
			// launch di engine.
			launch();

		}

		/**
		 * @param args
		 */
		public void launch() {

			// Launch consumer group runner.
			if (system == null)
				throw new IllegalArgumentException();

			system = system.trim().toLowerCase();
			LOGGER.info("Launching ->" + system);
			Properties config = configure();
			if (!Boolean.parseBoolean(config.getProperty("running"))) {
				throw new IllegalArgumentException();
			}

			// fetch the configured consumer(s) and launch the
			// consumers. After successfully launching the
			// consumer chain, we will start the default
			// publisher, if configured.

			try

			{
				String[] names = config.getProperty(system + ".consumer.names").split(",");
				for (String name : names) {
					String clazz = config.getProperty(system + ".consumer." + name);
					ConsumerService c = (ConsumerService) Class.forName(clazz).newInstance();
					c.name(name);
					c.launch(config);
				}

				String clazz = config.getProperty(system + ".message.publisher");
				if (clazz != null) {
					Publisher p = (Publisher) Class.forName(clazz).newInstance();
					p.publish(config);
				}

			} catch (Throwable e)

			{
				// exit.
				LOGGER.error(e.getMessage(), e);
				HaltJvm.exit(-1, 5000);
			}

		}

	}

}
