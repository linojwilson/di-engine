/**
 * 
 */
package com.testing.di.core.readers.kafka;

import java.util.Properties;

/**
 * @author linoj.wilson
 *
 */
public interface ConsumerService {
	
	public void launch(Properties config);
	public void name(String name);
	

}
