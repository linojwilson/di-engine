/**
 * 
 */
package com.testing.di.core.readers.kafka;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.CommitFailedException;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.testing.di.core.processors.Handler;
import com.testing.di.util.HaltJvm;

/**
 * @author linoj.wilson
 *
 */
public class MessageConsumer implements Runnable {
	
	private static final Logger log = LoggerFactory.getLogger(MessageConsumer.class);
	private Consumer<String, byte[]> consumer;

	private Properties config;
	private String topic;
	private String id;
	private String name;
	private String system;

	public MessageConsumer(Properties config, String name, String id) {
		this.config = config;
		this.system = config.getProperty("system");
		this.name = name;
		this.id = id;
	}

	@Override
	public void run() {
		
		try

		{

			// we will create kafka consumer
			String partialKey = system +".consumer." + name;
			this.topic = config.getProperty( partialKey + ".topic");
			log.info("Launching " + system + " consumer with id=" + id 
					+ " to process messages in topic=" + topic );
			
			String clazz = config.getProperty(partialKey + ".handler");
			if ( clazz == null) {
				//exit
				HaltJvm.exit(-1,5000);
			}
			Handler h =  null;
			try
			{
				h = (Handler) Class.forName(clazz).newInstance();
				h.configure(config);
				h.setName(name);
				h.setId(id);
			}
			
			catch ( Throwable t)
			{
				log.error(t.getMessage(),t);
				HaltJvm.exit(-1, 5000);
			}
			
			Properties consumerConfig = new Properties();
			String clientId = system + "-" + name + "-" + id;
			consumerConfig.put("client.id", clientId);
			
			// we don't want kafka issue WARN for properties kafka
			// do not understand. Let's fish out kafka properties
			// and launch the consumer.
			load(consumerConfig);
			consumer = new KafkaConsumer<>(consumerConfig);
			consumer.subscribe(Collections.singletonList(topic));
			
			while (true) {
				// idle wait till we hear from the broker.
				final ConsumerRecords<String, byte[]> records = consumer.poll(Long.MAX_VALUE);
				if (records.count() != 0) {

					try {
						for (TopicPartition partition : records.partitions()) {
							List<ConsumerRecord<String, byte[]>> partitionRecords = records.records(partition);
							for (ConsumerRecord<String, byte[]> record : partitionRecords) {
								// we will invoke router handler clazz with configured
								// parameters.
								log.info("Client ID=" + this.id 
										+ "Consumer Record:key=" + record.key()
										+ " partition=" + record.partition()
										+ "offset=" + record.offset());
								log.info("Key=" + record.key());
								h.handle(record.key(), record.value());
							}
								
								
							consumer.commitAsync(new OffsetCommitCallback() {
						      @Override
						      public void onComplete(Map<TopicPartition, OffsetAndMetadata> offsets, 
						                             Exception exception) {
						        if (exception != null) {
						          //TODO application specific failure handling
						         log.error(exception.getMessage(), exception);
						        }
						      }
						    });
							
						}
					
					} catch (CommitFailedException e) {
						log.error(e.getMessage(), e);
						// application specific failure handling
					}
				}

			}
		}

		catch (WakeupException w)

		{
			// ignored, it's a shutdown request
		}
		
		catch ( Throwable e)
		
		{
			log.error(e.getMessage(),e);
		}

		finally

		{
			if (consumer != null)
				consumer.close();
		}

	}

	private void load(Properties consumerConfig) {
		// fish out the kafka configuration.
		consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, 
				config.getProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG));
		consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, 
				config.getProperty(ConsumerConfig.GROUP_ID_CONFIG));
		consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, 
				config.get(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG));
		consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, 
				config.getProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG));
		
		
	}

	public void shutdown() {
		if (consumer != null)
			consumer.wakeup();
	}
	
	

}
