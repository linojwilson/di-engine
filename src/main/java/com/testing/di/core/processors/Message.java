/**
 * 
 */
package com.testing.di.core.processors;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.testing.di.core.validators.ValidationError;

/**
 * @author linoj.wilson
 *
 */
public class Message {
	
	private String msgType;
	
	// we will take path for now. When we
	// process using queues, this field
	// will contain actual message.
		
	private String msgContents;
	private Long periodId;
	private String payloadType;
	private String msgId;
	private String system;
	private String sourceType;
	private String status;
	private String details;
	private String path;
	private String key;
	private String recordGuid;
	private byte [] data;
	private List<ValidationError> validationErrors;
	
	public Message() {
		validationErrors = new ArrayList<ValidationError>();
		
	}
	
	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}
	/**
	 * @param msgType the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	/**
	 * @return the msgContents
	 */
	public String getMsgContents() {
		return msgContents;
	}
	/**
	 * @param msgContents the msgContents to set
	 */
	public void setMsgContents(String msgContents) {
		this.msgContents = msgContents;
	}
	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}
	/**
	 * @param periodId the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}
	/**
	 * @return the msgId
	 */
	public String getMsgId() {
		return msgId;
	}
	/**
	 * @param msgId the msgId to set
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	/**
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}
	/**
	 * @param system the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}
	/**
	 * @return the sourceType
	 */
	public String getSourceType() {
		return sourceType;
	}
	/**
	 * @param sourceType the sourceType to set
	 */
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	/**
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return the payloadType
	 */
	public String getPayloadType() {
		return payloadType;
	}
	/**
	 * @param payloadType the payloadType to set
	 */
	public void setPayloadType(String payloadType) {
		this.payloadType = payloadType;
	}
	/**
	 * @return the key
	 */
	public String getKey() {
		
		// we will generate UUID, if key is null.
		if ( key == null) {
			key = UUID.randomUUID()
					.toString();
		}
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the recordGuid
	 */
	public String getRecordGuid() {
		return recordGuid;
	}
	/**
	 * @param recordGuid the recordGuid to set
	 */
	public void setRecordGuid(String recordGuid) {
		this.recordGuid = recordGuid;
	}
	
	public void addValidationError(ValidationError error) {
		validationErrors.add(error);
	}

	/**
	 * @return the validationErrors
	 */
	public List<ValidationError> getValidationErrors() {
		return validationErrors;
	}

	/**
	 * @param validationErrors the validationErrors to set
	 */
	public void setValidationErrors(List<ValidationError> validationErrors) {
		this.validationErrors = validationErrors;
	}
}
