/**
 * 
 */
package com.testing.di.core.processors;

import java.util.Properties;

/**
 * @author linoj.wilson
 *
 */
public interface Handler {

	/**
	 * configure the handler
	 * 
	 * @param config
	 * @throws Throwable 
	 */
	public void configure(Properties config) throws Throwable;
	
	/**
	 * run the handler.
	 * @param key TODO
	 * @param message
	 * 
	 * @throws Throwable
	 */
	public void handle(String key, byte [] message) throws Throwable;
	
	/**
	 * unique identifier.
	 * 
	 * @param id
	 */
	public void setId(String id);
	
	/**
	 * unique name
	 * 
	 * @param name
	 */
	public void setName(String name);
}
