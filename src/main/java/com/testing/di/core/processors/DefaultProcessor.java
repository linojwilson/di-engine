/**
 * 
 */
package com.testing.di.core.processors;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.transform.stream.StreamSource;

import org.milyn.Smooks;
import org.milyn.container.ExecutionContext;
import org.milyn.payload.JavaResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.testing.di.core.validators.Validator;

/**
 * @author linoj.wilson
 *
 */
public class DefaultProcessor extends AbstractProcessor {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultProcessor.class);
	
	/* (non-Javadoc)
	 * @see com.testing.bas.core.readers.kafka.AbstractProcessor#handle(com.testing.bas.core.Message, java.lang.String)
	 */
	@Override
	public void handle(Message msg, String xml) throws Throwable {
		Smooks smooks = null;

		try

		{
			String partialKey = msg.getSystem() + "." + msg.getMsgType().trim().toLowerCase();
			String enrich = config
					.getProperty( partialKey + ".jpa.mapping");
			if (enrich == null) {
				// we don't have jpa mapping. Okay, let's move on!
				return;
			}
			smooks = new Smooks(enrich);
			ExecutionContext executionContext = smooks.createExecutionContext();
			JavaResult result = new JavaResult();
			smooks.filterSource(executionContext, new StreamSource(new ByteArrayInputStream(xml.getBytes())), result);

			// fetch the custom delete handlers for beans and run
			// the custom handlers.
			
			String customBeanIds = config.getProperty( partialKey + ".custom.delete.beanIds");
			Map<String,Boolean> beanLookup = new HashMap<String,Boolean>();
			if ( customBeanIds != null){
				for ( String beanId: customBeanIds.split(",")){
					beanLookup.put(beanId, Boolean.TRUE);
				}
				
			}
						
			// do we purge the existing data for the message?
			String deleteClazz = config.getProperty("engine.jpa.delete.handler");
			if (deleteClazz != null) {
				PurgeHandler p = (PurgeHandler) Class.forName(deleteClazz).newInstance();
				p.init(config, beanLookup);
				p.purge(result.getResultMap(), msg);

			}

			// setup record key
			Object oz = result.getResultMap().get("RecordKey");
			if (oz != null) {
				KeyFinder finder = (KeyFinder) oz;
				msg.setKey(finder.getRecordKey());
				result.getResultMap().remove("RecordKey");
			}

			// do we have any validators to run?
			validate(msg, result.getResultMap());

			
			Set<Entry<String, Object>> beans = result.getResultMap().entrySet();
			for (Entry<String, Object> bean : beans) {
				// persist beans

				try

				{
					if (bean.getValue() != null) 
						
					{
						Object o = bean.getValue();
						save(o, msg);
					}
					
				}

				catch (Throwable t)

				{
					LOGGER.error(t.getMessage(), t);
				}

			}

		}

		finally

		{
			if (smooks != null)
				smooks.close();
		}

	}
	
	/**
	 * do any validations as per configuration
	 * 
	 * @param msg
	 * @param results
	 * @throws Throwable
	 */
	private void validate(Message msg, Map<String, Object> results) throws Throwable {
		String validations = config
				.getProperty(msg.getSystem() + "." + msg.getMsgType().trim().toLowerCase() + ".validator.beans");

		if (validations == null)
			return;
		String[] beans = validations.split(",");
		for (String beanId : beans) {

			Object o = results.get(beanId);
			if (o != null) {
				((Validator) o).validate(msg);
				results.remove(beanId);

			}

		}

	}

}
