/**
 * 
 */
package com.testing.di.core.processors;

/**
 * @author linoj.wilson
 *
 */
public interface KeyFinder {
	
	public String [] getKeyFields();
	public String getRecordKey();

}
