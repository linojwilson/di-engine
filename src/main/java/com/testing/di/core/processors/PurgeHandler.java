/**
 * 
 */
package com.testing.di.core.processors;

import java.util.Map;
import java.util.Properties;

/**
 * @author linoj.wilson
 *
 */
public interface PurgeHandler {
	
	public void purge(Map<String,Object> beans, Message msg) throws Throwable;
	public void init(Properties properities, Map<String, Boolean> customDeleteHandlers);
	

}
