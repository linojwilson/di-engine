/**
 * 
 */
package com.testing.di.core.processors;

import java.io.ByteArrayInputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.testing.di.core.audit.Auditor;
import com.testing.di.core.audit.Record;
import com.testing.di.core.validators.SourceTypes;
import com.testing.di.core.validators.ValidationError;
import com.testing.di.core.validators.ValidationErrors;
import com.testing.di.core.validators.ValidationTypes;
import com.testing.di.core.validators.XmlValidator;
import com.testing.di.core.validators.XslValidator;
import com.testing.di.core.validators.errors.ValidatorExecution;
import com.testing.di.util.ConnectionManager;
import com.testing.di.util.JaxbInitializer;

/**
 * @author linoj.wilson
 *
 */
public abstract class AbstractProcessor implements Handler {
	
	private static Logger log = LoggerFactory
			.getLogger(AbstractProcessor.class);
	
	protected String name;
	protected String id;
	protected Properties config;

	/* (non-Javadoc)
	 * @see com.testing.bas.core.readers.kafka.Handler#configure(java.util.Properties)
	 */
	@Override
	public void configure(Properties config) throws Throwable {
		this.config = config;

	}

	/* (non-Javadoc)
	 * @see com.testing.bas.core.readers.kafka.Handler#handle(java.lang.String, byte[])
	 */
	@Override
	public void handle(String key, byte[] message) throws Throwable {
		
		//TODO handle statistics count stuff.
		
		// do we have payload in the request?
		if (( message == null) || (message.length <= 0)) return;
		log.info("Key=" + key);
		
		// prepare message payload and shove the
		// payload down the pipeline.
		Message msg = new Message();
		msg.setMsgId(key);
		msg.setData(message);
		msg.setSystem(config.getProperty("system"));
		
		// TODO period should be part of the message. for now let's get
		// from the configuration.
		Long periodId = Long.parseLong(config.getProperty( config.getProperty("system")  
				+ ".message.periodId"));
		msg.setPeriodId(periodId);
		
		String sourceType = config.getProperty(config.getProperty("system")  
				+ ".message.sourceType")
				.toLowerCase();
		msg.setSourceType(sourceType);
		msg.setPayloadType(config.getProperty(config.getProperty("system") +  ".message.format"));
		
		// TODO we will need to get message type in the message. Currently
		// we don't have it. First let's get the system work as before
		// and then worry about this. The change has to happen at source
		// which sends the messages i.e it should come from LLYOD
		// systems or based on the split we do, we might have add the
		// message node into the XML. 
		String msgType = config.getProperty(config.getProperty("system")
				+ ".message.type").trim().toLowerCase();
		msg.setMsgType(msgType);
		List<Record> records = new ArrayList<Record>();
		Record record = new Record();
		
		try
		
		{
			
			XmlValidator validator = null;
			
			//setup the XSD validator, if it's configured.
			String clazz = config.getProperty(msg.getSystem() + ".xml.validator");
			if (clazz != null) 
			{
				validator = (XmlValidator) Class.forName(clazz).newInstance();
				validator.init(config.getProperty(msg.getSystem() 
						+ ".message.xsd"));
			}
			
			// let it rip!
			String xml = new String(msg.getData(), "UTF-8");
			boolean xmlValid = true;
			
			List<ValidationError> validationErrors = new ArrayList<ValidationError>();
			
			// do we have semantically correct xml
			// payload?
		
			record.setGuid(msg.getKey());
			record.setMsgId(msg.getMsgId());
			record.setProcessTime(new Date(System.currentTimeMillis()));
			record.setXml(xml);
			record.setStatus(ValidationTypes.SUCCESS.toString());
			records.add(record);
			if (validator != null) {
				ValidationErrors e = null;
				
				try
				
				{
					e = validator.validate(xml, msg);
					if (e.getCount() > 0)

					{

						validationErrors.addAll(e.getValidationErrors());
						msg.setStatus(ValidationTypes.PROCESSING_ERROR.name());
						msg.setDetails(null);
						Auditor.getValidationLogger().log(validationErrors, msg);
						xmlValid = false;

					}
				}
				
				catch (Throwable t)
				
				{
					xmlValid = false;
					msg.setStatus(ValidationTypes.PROCESSING_ERROR.name());
					msg.setDetails(t.getMessage());
					Auditor.getMessageAuditor().audit(msg);
					
				}
				

			}
			
			if ( !xmlValid ) return;
			
			// XSL Validation?
			boolean hasBusinessErrors = false;
			// do we have xsl validations/transformations?
			String xslPath = config.getProperty(msg.getSystem() + ".message.validation." + msgType);
			if ((xmlValid) && (xslPath != null)) {
				String transformedXml = XslValidator.transform(msg.getData(), xslPath);
				ValidationErrors context = getValidationErrors(transformedXml, msg);
				if (context.getValidationErrors().size() > 0) {
					hasBusinessErrors = true;
					validationErrors.addAll(context.getValidationErrors());

				}
			}
			
			record.setKey(msg.getKey());
			handle(msg, xml);
			
			if ( msg.getValidationErrors().size() > 0) {
				if (! hasBusinessErrors ) {
					//FIXME we will need handle statistics stuff.
				}
				validationErrors.addAll(msg.
						getValidationErrors());
			}
			
			// we might have "business key" updated as a
			// result of transformation. So we will now
			// iterate through the stuff and set the requisite
			// keys again.
			for (ValidationError e : validationErrors) {
				e.setGuid(msg.getKey());
			}

			for (Record r : records) {
				r.setGuid(msg.getKey());
				r.setKey(msg.getKey());
			}
			
			// do we have validation errors?
			if ( validationErrors.size() > 0) 
			{
				record.setStatus(ValidationTypes.PROCESSING_ERROR.toString());
				
			}
			
			Auditor.getValidationLogger().log(validationErrors, msg);
			Auditor.getRecordLogger().audit(records);
			
		}
		
		catch ( Throwable t)
		
		{
			log.error(t.getMessage(), t);

			try {
				msg.setStatus(ValidationTypes.PROCESSING_ERROR.name());
				msg.setDetails(t.getMessage());
				Auditor.getMessageAuditor().update(msg);
			} catch (Throwable e1) {
				// ignored.
			}
		}
		
		
		

	}
	
	protected <T> void save(T t, Message msg) throws Throwable {

		if (t instanceof List) {

			@SuppressWarnings("unchecked")
			List<T> x = (List<T>) t;
			save(x, msg);

		}

		else {

			EntityManager manager = ConnectionManager.getInstance().getJpaConnection();
			manager.getTransaction().begin();
			manager.merge(t);

			manager.flush();
			manager.getTransaction().commit();
			manager.close();

		}
	}

	protected <T> void save(List<T> o, Message msg) throws Throwable {
		
		String bS = config.getProperty(config.getProperty("system") + ".save.batchSize");
		int batchSize = (bS == null)? 1000:Integer.parseInt(bS);
		int i = 0;
		EntityManager manager = ConnectionManager.getInstance().getJpaConnection();
		manager.getTransaction().begin();
		for (T c : o) {

			manager.merge(c);
			if (++i % batchSize == 0) {
				// records.
				manager.flush();
				manager.clear();
			}
		}

		manager.flush();
		manager.getTransaction().commit();
		manager.close();

	}
	
	public abstract void handle(Message msg, String xml) throws Throwable;

	/* (non-Javadoc)
	 * @see com.testing.bas.core.readers.kafka.Handler#setId(java.lang.String)
	 */
	@Override
	public void setId(String id) {
		this.id = id;

	}

	/* (non-Javadoc)
	 * @see com.testing.bas.core.readers.kafka.Handler#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;

	}
	
	/**
	 * decipher validation errors
	 * 
	 * @param xml
	 * @param msg
	 * @return
	 * @throws Throwable
	 */
	protected ValidationErrors getValidationErrors(String xml, Message msg) throws Throwable {

		// setup execution context
		ValidationErrors context = new ValidationErrors();
		context.setMsgId(msg.getMsgId());
		context.setMsgType(msg.getMsgType());
		context.setPeriodId(msg.getPeriodId());
		context.setSystem(msg.getSystem());

		Unmarshaller jaxbUnMarshaller = JaxbInitializer.getJaxbContext().createUnmarshaller();
		Source xmlSource = new StreamSource(new ByteArrayInputStream(xml.toString().getBytes("UTF-8")));
		ValidatorExecution validations = (ValidatorExecution) jaxbUnMarshaller.unmarshal(xmlSource);
		if (validations.getValidatorResult() != null) {

			for (ValidatorExecution.ValidatorResult r : validations.getValidatorResult()) {
				ValidationError v = new ValidationError();
				v.setMsgId(msg.getMsgId());
				v.setGuid(msg.getKey());
				v.setMsgType(msg.getMsgType());
				v.setType(ValidationTypes.BUSINESS_TYPE.toString());
				v.setCode(String.valueOf(r.getRuleReference()));
				v.setDesc(r.getResultIndicator());
				v.setMsg(r.getResultDescription());
				v.setSource(SourceTypes.ACCORD_SOURCE.toString());
				context.addValidationError(v);

			}

		}

		return context;
	}

}
