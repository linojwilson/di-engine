/**
 * 
 */
package com.testing.di.core.processors;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.testing.di.core.audit.Auditor;
import com.testing.di.core.readers.kafka.MessageSender;
import com.testing.di.core.validators.ValidationTypes;

/**
 * @author linoj.wilson
 *
 */
public class DefaultRouter implements Handler {
	
	private static Logger log = LoggerFactory
			.getLogger(DefaultRouter.class);

	private Properties config;
	private String name;
	private String id;
	private Collection<XPathExpression> exps;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.testing.bas.core.readers.kafka.Handler#configure(java.util.Properties)
	 */
	@Override
	public void configure(Properties config) throws Throwable {
		this.config = config;
		
		//prepare XPath expressions for later.
		String [] elements = config.getProperty("dasat.message.splitter.element")
				.split(",");
		this.exps = new ArrayList<XPathExpression>();
		XPath xPath = XPathFactory.newInstance().newXPath();
		for ( String element: elements) {
			this.exps.add(xPath.compile(element));
		}
		

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.testing.bas.core.readers.kafka.Handler#handle(byte[])
	 */
	@Override
	public void handle(String key, byte[] message) throws Throwable {
		
		Message msg = new Message();
		
		try
		
		{
			
			msg.setMsgId(key);
			msg.setStatus(ValidationTypes.SUCCESS.name());
			msg.setDetails(null);
			msg.setSystem(config.getProperty("system"));
			
			// TODO period should be part of the message. for now let's get
			// from the configuration.
			Long periodId = Long.parseLong(config.getProperty( config.getProperty("system")  
					+ ".message.periodId"));
			msg.setPeriodId(periodId);
			
			// TODO we will need to get message type in the message. Currently
			// we don't have it. First let's get the system work as before
			// and then worry about this. The change has to happen at source
			// which sends the messages i.e it should come from LLYOD
			// systems or based on the split we do, we might have add the
			// message node into the XML. 
			String msgType = config.getProperty(config.getProperty("system")
					+ ".message.type").trim().toLowerCase();
			msg.setMsgType(msgType);
			
			Document xml = DocumentBuilderFactory.newInstance().newDocumentBuilder()
					.parse(new ByteArrayInputStream(message));
			
			if ( exps.isEmpty()) throw new IllegalStateException();
			NodeList nl = null;
			Iterator<XPathExpression> i = this.exps.iterator();
			boolean done = false;
			while ( (!done) && ( i.hasNext())) {
				nl = (NodeList) i.next().
						evaluate(xml, XPathConstants.NODESET);
				if ( nl != null) done = true;
			}
			
			if ( ! done ) {
				Auditor.getMessageAuditor().update(msg);
				return;
			}
		
			//process split nodes and send to configured topic.
			MessageSender sender = new MessageSender();
			String topic = config.getProperty("dasat.message.splitter.topic");
			sender.setTopic(topic);
			// generate unique client-id for sending messages.
			String clientId = config.getProperty(config.getProperty("system") 
					+ ".consumer." + this.name + ".id")  + "-" + this.id;
			config.put("producer.id", clientId);
			
			sender.configure(config);
			Auditor.getMessageAuditor().update(msg);
			
			for (int m = 0; m < nl.getLength(); m++) 
			{
				Node node = nl.item(m);
				StringWriter writer = new StringWriter();
				Transformer xform = TransformerFactory.newInstance().newTransformer();
				xform.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				xform.transform(new DOMSource(node), new StreamResult(writer));
				String fragment = writer.toString();
				sender.send(key,fragment.getBytes());
			}
			
			// close producer.
			sender.close();
			
		}
		
		catch ( Throwable e)
		
		{
			log.error(e.getMessage(),e);
			msg.setStatus(ValidationTypes.PROCESSING_ERROR.name());
			msg.setDetails(e.getMessage());
			Auditor.getMessageAuditor().update(msg);
		}
		

	}

	@Override
	public void setId(String id) {
		this.id = id;
		
	}

	@Override
	public void setName(String name) {
		this.name = name;
		
	}

}
