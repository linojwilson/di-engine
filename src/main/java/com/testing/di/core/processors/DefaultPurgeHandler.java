/**
 * 
 */
package com.testing.di.core.processors;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.testing.di.util.ConnectionManager;

/**
 * @author linoj.wilson
 *
 */
public class DefaultPurgeHandler implements PurgeHandler {

	private static final Logger logger = LoggerFactory.getLogger(DefaultPurgeHandler.class);

	private Properties config;
	private Map<String,Boolean> customDeleteHandlers;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.testing.bas.core.DeleteHandler#delete()
	 */
	@Override
	public void purge(Map<String, Object> beans, Message msg) throws Throwable {
		String partialKey = msg.getSystem() + "." + msg.getMsgType().trim().toLowerCase();
		boolean delete = Boolean.parseBoolean(config.getProperty(partialKey + ".delete"));
		if (delete) {
			// delete the information in tables.
			String dml = config.getProperty(partialKey + ".jpa.dml");
			if (dml == null)
				throw new IllegalArgumentException();
			Properties dmlStatements = new Properties();
			dmlStatements.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(dml));
			String keyRef = dmlStatements.getProperty(partialKey + ".delete.key-ref");
			if (keyRef == null)
				throw new IllegalArgumentException();
			Object o = beans.get(keyRef);

			// we will now use reflection to get information
			// at runtime and fire configured delete dml.
			if (o == null) {
				throw new IllegalStateException();
			}

			String keyFields = dmlStatements.getProperty(partialKey + ".jpa.key-fields");
			if (keyFields == null) {
				throw new IllegalArgumentException();
			}
			String[] fieldNames = keyFields.split(",");
			List<Field> fields = new ArrayList<Field>();
			for (String fn : fieldNames) {
				Field field = o.getClass().getDeclaredField(fn);
				field.setAccessible(true);
				fields.add(field);
			}

			EntityManager manager = null;

			try

			{
				manager = ConnectionManager.getInstance().getJpaConnection();
				for (Entry<String, Object> e : beans.entrySet()) {

					if (e.getValue() != null) {
						boolean doDelete = (customDeleteHandlers.get(e.getKey()) == null) ? true : false;
						if (doDelete) {

							String sql = dmlStatements.getProperty(partialKey + ".delete." + e.getKey());
							if (sql != null) {
								EntityTransaction et = null;
								Query query = null;

								try {
									et = manager.getTransaction();
									et.begin();
									query = manager.createNamedQuery(sql);
									int i = 1;
									for (Field f : fields) {
										String y = dmlStatements.getProperty(
												partialKey + ".delete." + e.getKey() + ".highLevelReference");
										boolean temporal = (y != null) ? Boolean.parseBoolean(y) : false;
										if (i > 1 && temporal) {
											query.setParameter(i++, f.get(o));
										} else if (i == 1) {
											query.setParameter(i++, f.get(o));
										}

									}

									// do we set periodId?
									String y = dmlStatements
											.getProperty(partialKey + ".delete." + e.getKey() + ".periodId");
									boolean temporal = (y != null) ? Boolean.parseBoolean(y) : false;
									Long periodId = (System.getProperty("periodId") != null)
											? Long.parseLong(System.getProperty("periodId"))
											: -1L;
									if (temporal) {
										query.setParameter(i++, periodId);
									}
									Object entity = query.getSingleResult();
									if (entity != null) {
										manager.remove(entity);
										manager.flush();
										et.commit();
									}

								} catch (NoResultException ex) {
									// do-nothing data not available for key combination
								} catch (Exception ex) {
									logger.error(ex.getMessage(), ex);
								}
							}

						}

					}

				}

			}

			catch (Throwable t)

			{
				logger.error(t.getMessage(), t);

			}

			finally

			{
				if (manager != null) {
					manager.close();
				}
			}

		}
	}

	@Override
	public void init(Properties config, Map<String, Boolean> customDeleteHandlers) {
		this.config = config;
		this.customDeleteHandlers = customDeleteHandlers;

	}

}
