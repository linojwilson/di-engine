/**
 * 
 */
package com.testing.di.core.audit.impl;

import java.util.List;

import javax.persistence.EntityManager;

import com.testing.di.core.audit.InboundRecord;
import com.testing.di.core.audit.Record;
import com.testing.di.core.audit.RecordAuditor;
import com.testing.di.util.ConnectionManager;

/**
 * @author linoj.wilson
 *
 */
public class RecordAuditorImpl implements RecordAuditor {
	
	private int batchSize = 10000;

	/* (non-Javadoc)
	 * @see com.testing.bas.core.audit.RecordAuditor#audit(com.testing.bas.core.audit.Record)
	 */
	@Override
	public void audit(List<Record> records) throws Throwable {
		int i = 0;
		EntityManager manager = ConnectionManager.getInstance()
				.getJpaConnection();
		manager.getTransaction().begin();		
		
		for (Record r : records) 
		{
			InboundRecord ir = new InboundRecord();
			ir.setMessageGuid(r.getMsgId());
			ir.setMessageRecordGuid(r.getGuid());
			ir.setMessageRecBusinessKeyRef(r.getKey());
			ir.setMessageRecordObject(r.getXml());
			ir.setProcessedDateTime(r.getProcessTime());
			ir.setRecordErrorDetails(r.getDetail());
			ir.setRecordStatus(r.getStatus());
			manager.merge(ir);
			if (++i % batchSize == 0) {
		      // records.
		      manager.flush();
		      manager.clear();
		    }
		}
		manager.flush();
		manager.getTransaction().commit();
		manager.close();	

	}

}
