/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.testing.di.core.audit;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author linoj.wilson
 */
@Entity
@Table(name = "MESSAGE_RECORDS_PROCESS_LOG")
public class InboundRecord implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "MESSAGE_RECORD_GUID")
    private String messageRecordGuid;
    @Column(name = "MESSAGE_GUID")
    private String messageGuid;
    @Column(name = "MESSAGE_REC_BUSINESS_KEY_REF")
    private String messageRecBusinessKeyRef;
    @Column(name = "PROCESSED_DATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date processedDateTime;
    @Column(name = "RECORD_STATUS")
    private String recordStatus;
    @Column(name = "RECORD_ERROR_DETAILS")
    private String recordErrorDetails;
    @Lob
    @Column(name = "MESSAGE_RECORD_OBJECT")
    private String messageRecordObject;

    public InboundRecord() {
    }

    public InboundRecord(String messageRecordGuid) {
        this.messageRecordGuid = messageRecordGuid;
    }

    public String getMessageRecordGuid() {
        return messageRecordGuid;
    }

    public void setMessageRecordGuid(String messageRecordGuid) {
        this.messageRecordGuid = messageRecordGuid;
    }

    public String getMessageGuid() {
        return messageGuid;
    }

    public void setMessageGuid(String messageGuid) {
        this.messageGuid = messageGuid;
    }

    public String getMessageRecBusinessKeyRef() {
        return messageRecBusinessKeyRef;
    }

    public void setMessageRecBusinessKeyRef(String messageRecBusinessKeyRef) {
        this.messageRecBusinessKeyRef = messageRecBusinessKeyRef;
    }

    public Date getProcessedDateTime() {
        return processedDateTime;
    }

    public void setProcessedDateTime(Date processedDateTime) {
        this.processedDateTime = processedDateTime;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getRecordErrorDetails() {
        return recordErrorDetails;
    }

    public void setRecordErrorDetails(String recordErrorDetails) {
        this.recordErrorDetails = recordErrorDetails;
    }

    public String getMessageRecordObject() {
        return messageRecordObject;
    }

    public void setMessageRecordObject(String messageRecordObject) {
        this.messageRecordObject = messageRecordObject;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (messageRecordGuid != null ? messageRecordGuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InboundRecord)) {
            return false;
        }
        InboundRecord other = (InboundRecord) object;
        if ((this.messageRecordGuid == null && other.messageRecordGuid != null) || (this.messageRecordGuid != null && !this.messageRecordGuid.equals(other.messageRecordGuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[ messageRecordGuid=" + messageRecordGuid + " ]";
    }
    
}
