/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.testing.di.core.audit;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author linoj.wilson
 */
@Entity
@Table(name = "MESSAGE_LOG")
public class InboundMessage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "MESSAGE_GUID")
    private String messageGuid;
    @Column(name = "MESSAGE_TYPE")
    private String messageType;
    @Column(name = "MESSAGE_FILE_NAME")
    private String messageFileName;
    @Column(name = "MESSAGE_SOURCE")
    private String messageSource;
    @Column(name = "MESSAGE_FORMAT")
    private String messageFormat;
    @Column(name = "PERIOD_ID")
    private Long periodId;
    @Column(name = "PROCESSED_DATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date processedDateTime;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "ERROR_DETAILS")
    private String errorDetails;
    @Lob
    @Column(name = "MESSAGE_OBJECT")
    private String messageObject;

    public InboundMessage() 
    
    {
    }

    public InboundMessage(String messageGuid) {
        this.messageGuid = messageGuid;
    }

    public String getMessageGuid() {
        return messageGuid;
    }

    public void setMessageGuid(String messageGuid) {
        this.messageGuid = messageGuid;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageFileName() {
        return messageFileName;
    }

    public void setMessageFileName(String messageFileName) {
        this.messageFileName = messageFileName;
    }

    public String getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(String messageSource) {
        this.messageSource = messageSource;
    }

    public String getMessageFormat() {
        return messageFormat;
    }

    public void setMessageFormat(String messageFormat) {
        this.messageFormat = messageFormat;
    }

    public Long getPeriodId() {
        return periodId;
    }

    public void setPeriodId(Long periodId) {
        this.periodId = periodId;
    }

    public Date getProcessedDateTime() {
        return processedDateTime;
    }

    public void setProcessedDateTime(Date processedDateTime) {
        this.processedDateTime = processedDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getMessageObject() {
        return messageObject;
    }

    public void setMessageObject(String messageObject) {
        this.messageObject = messageObject;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (messageGuid != null ? messageGuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InboundMessage)) {
            return false;
        }
        InboundMessage other = (InboundMessage) object;
        if ((this.messageGuid == null && other.messageGuid != null) || (this.messageGuid != null && !this.messageGuid.equals(other.messageGuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "j[ messageGuid=" + messageGuid + " ]";
    }
    
}
