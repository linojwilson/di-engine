/**
 * 
 */
package com.testing.di.core.audit;

import com.testing.di.core.processors.Message;

/**
 * @author linoj.wilson
 *
 */
public interface MessageAuditor {
	
	public void audit(Message message) throws Throwable;
	public void update(Message message) throws Throwable;
	public void updateStatus(Message message) throws Throwable;

}
