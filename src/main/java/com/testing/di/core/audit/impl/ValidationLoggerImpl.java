/**
 * 
 */
package com.testing.di.core.audit.impl;

import java.util.List;

import javax.persistence.EntityManager;

import com.testing.di.core.audit.Validation;
import com.testing.di.core.audit.ValidationLogger;
import com.testing.di.core.processors.Message;
import com.testing.di.core.validators.ValidationError;
import com.testing.di.util.ConnectionManager;

/**
 * @author linoj.wilson
 *
 */
public class ValidationLoggerImpl implements ValidationLogger {
	
	private int batchSize = 10000;

	/* (non-Javadoc)
	 * @see com.testing.bas.core.audit.ValidationLogger#log(java.util.List)
	 */
	@Override
	public void log(List<ValidationError> validations, Message msg) throws Throwable {
		int i = 0;
		EntityManager manager = ConnectionManager.getInstance()
				.getJpaConnection();
		manager.getTransaction().begin();		
		
		int id = 1;
		for (ValidationError s : validations) 
		{
			Validation v = new Validation();
			v.setMessageRecordGuid(s.getGuid());
			v.setValidationCode(s.getCode());
			v.setValidationSequence(s.getGuid() 
					+ "|" + id++);
			v.setValidationCodeDescription(s.getDesc());
			v.setValidationReason(s.getMsg());
			v.setValidationType(s.getType());
			v.setMsgId(msg.getMsgId());
			v.setSource(s.getSource());
			manager.merge(v);
			if (++i % batchSize == 0) {
		      // records.
		      manager.flush();
		      manager.clear();
		    }
		}
		manager.flush();
		manager.getTransaction().commit();
		manager.close();	

	}

}
