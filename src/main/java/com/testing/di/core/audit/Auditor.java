/**
 * 
 */
package com.testing.di.core.audit;

import java.util.Properties;

/**
 * @author linoj.wilson
 *
 */
public class Auditor {
	
	private static RecordAuditor recordLogger;
	private static ValidationLogger validationLogger;
	private static MessageAuditor messageAuditor;
	
	
	/**
	 * @return the recordLogger
	 */
	public static RecordAuditor getRecordLogger() {
		return recordLogger;
	}

	/**
	 * @return the validationLogger
	 */
	public static ValidationLogger getValidationLogger() {
		return validationLogger;
	}

	/**
	 * @return the messageAuditor
	 */
	public static MessageAuditor getMessageAuditor() {
		return messageAuditor;
	}

	public static void init(Properties config) throws Throwable {
		
		recordLogger = (RecordAuditor) Class.forName(config.
				getProperty("engine.record.auditor")).newInstance();
		validationLogger = (ValidationLogger) Class.forName(config.
				getProperty("engine.validator.auditor")).newInstance();
		messageAuditor = (MessageAuditor) Class.forName(config.
				getProperty("engine.message.auditor")).newInstance();
	}
	
}
