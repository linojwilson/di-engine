/**
 * 
 */
package com.testing.di.core.audit;

import java.util.List;

import com.testing.di.core.processors.Message;
import com.testing.di.core.validators.ValidationError;

/**
 * @author linoj.wilson
 *
 */
public interface ValidationLogger {
	
	public void log(List<ValidationError> validations,
			Message msg) throws Throwable;

}
