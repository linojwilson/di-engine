/**
 * 
 */
package com.testing.di.core.audit.impl;

import java.sql.Date;

import javax.persistence.EntityManager;

import org.apache.commons.io.IOUtils;

import com.testing.di.core.audit.InboundMessage;
import com.testing.di.core.audit.MessageAuditor;
import com.testing.di.core.processors.Message;
import com.testing.di.core.validators.ValidationTypes;
import com.testing.di.util.ConnectionManager;

/**
 * @author linoj.wilson
 *
 */
public class MessageAuditorImpl implements MessageAuditor {

	/* (non-Javadoc)
	 * @see com.testing.bas.core.audit.MessageAuditor#audit(com.testing.bas.core.audit.Message)
	 */
	@Override
	public void audit(Message message) throws Throwable {
		
		// prepare inbound message
		InboundMessage inbound = new InboundMessage();
		inbound.setProcessedDateTime(new Date(System.currentTimeMillis()));
		inbound.setStatus(message.getStatus());
		inbound.setErrorDetails(message.getDetails());
		inbound.setMessageFileName(message.getPath());
		inbound.setMessageGuid(message.getMsgId());
		inbound.setMessageSource(message.getSystem());
		inbound.setPeriodId(message.getPeriodId());
		inbound.setMessageType(message.getMsgType());
		inbound.setMessageFormat(message.getPayloadType());
		inbound.setMessageObject(IOUtils.toString(message.getData(), "UTF-8"));
		
		EntityManager manager = ConnectionManager.getInstance()
				.getJpaConnection();
		manager.getTransaction().begin();
		manager.merge(inbound);
		manager.flush();
		manager.getTransaction().commit();
		manager.close();	

	}

	@Override
	public void update(Message message) throws Throwable {
		EntityManager manager = ConnectionManager.getInstance()
				.getJpaConnection();
		InboundMessage inbound = manager.find(InboundMessage.class, message.getMsgId());
		if ( inbound == null) {
			manager.close();
			audit(message);
			return;
		}
		inbound.setProcessedDateTime(new Date(System.currentTimeMillis()));
		inbound.setStatus(message.getStatus());
		inbound.setErrorDetails(message.getDetails());
		
		inbound.setMessageGuid(message.getMsgId());
		inbound.setMessageSource(message.getSystem());
		inbound.setPeriodId(message.getPeriodId());
		inbound.setMessageType(message.getMsgType());
		manager.getTransaction().begin();
		manager.merge(inbound);
		manager.flush();
		manager.getTransaction().commit();
		manager.close();	
		
	}

	@Override
	public void updateStatus(Message message) throws Throwable {
		EntityManager manager = ConnectionManager.getInstance()
				.getJpaConnection();
		InboundMessage inbound = manager.find(InboundMessage.class, message.getMsgId());
		if ( inbound == null) throw new IllegalStateException();
		if ( inbound.getStatus().equalsIgnoreCase(ValidationTypes
				.PROCESSING_ERROR.name())) {
			return;
		}
		inbound.setProcessedDateTime(new Date(System.currentTimeMillis()));
		inbound.setStatus(message.getStatus());
		inbound.setErrorDetails(message.getDetails());
		
	}

}
