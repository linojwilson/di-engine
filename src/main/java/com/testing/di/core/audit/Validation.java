/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.testing.di.core.audit;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author linoj.wilson
 */
@Entity
@Table(name = "MESSAGE_VALIDATION_LOG")
public class Validation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "MESSAGE_RECORD_GUID")
    private String messageRecordGuid;
    @Id
    @Column(name = "VALIDATION_SEQUENCE")
    private String validationSequence;
    @Column(name = "VALIDATION_TYPE")
    private String validationType;
    @Column(name = "VALIDATION_CODE")
    private String validationCode;
    @Column(name = "VALIDATION_CODE_DESCRIPTION")
    private String validationCodeDescription;
    @Column(name = "VALIDATION_REASON")
    private String validationReason;
    
    @Column(name = "VALIDATION_SOURCE")
    private String source;
    
    @Column(name = "MESSAGE_GUID")
    private String msgId;

    public Validation() {
    }

    public Validation(String validationSequence) {
        this.validationSequence = validationSequence;
    }

    public String getMessageRecordGuid() {
        return messageRecordGuid;
    }

    public void setMessageRecordGuid(String messageRecordGuid) {
        this.messageRecordGuid = messageRecordGuid;
    }

    public String getValidationSequence() {
        return validationSequence;
    }

    public void setValidationSequence(String validationSequence) {
        this.validationSequence = validationSequence;
    }

    public String getValidationType() {
        return validationType;
    }

    public void setValidationType(String validationType) {
        this.validationType = validationType;
    }

    public String getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(String validationCode) {
        this.validationCode = validationCode;
    }

    public String getValidationCodeDescription() {
        return validationCodeDescription;
    }

    public void setValidationCodeDescription(String validationCodeDescription) {
        this.validationCodeDescription = validationCodeDescription;
    }

    public String getValidationReason() {
        return validationReason;
    }

    public void setValidationReason(String validationReason) {
        this.validationReason = validationReason;
    }

    /**
	 * @return the msgId
	 */
	public String getMsgId() {
		return msgId;
	}

	/**
	 * @param msgId the msgId to set
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (validationSequence != null ? validationSequence.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Validation)) {
            return false;
        }
        Validation other = (Validation) object;
        if ((this.validationSequence == null && other.validationSequence != null) || (this.validationSequence != null && !this.validationSequence.equals(other.validationSequence))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication2.dto.MessageRecordsValidationLog[ validationSequence=" + validationSequence + " ]";
    }
    
}
