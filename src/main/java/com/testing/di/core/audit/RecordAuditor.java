/**
 * 
 */
package com.testing.di.core.audit;

import java.util.List;

/**
 * @author linoj.wilson
 *
 */
public interface RecordAuditor {
	
	public void audit(List<Record> records) throws Throwable;

}
