/**
 * 
 */
package com.testing.di.core.validators;

/**
 * @author linoj.wilson
 *
 */
import java.io.InputStream;

import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

public class ResourceResolver implements LSResourceResolver {

	private String prefix;

	public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {

		if (( prefix != null)  && ( prefix.trim().length() > 0)){
			systemId = prefix +"/" + systemId; 
		}
		
		InputStream resourceAsStream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(systemId);
		return new XsdLSInput(publicId, systemId, resourceAsStream);
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @param prefix
	 *            the prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

}
