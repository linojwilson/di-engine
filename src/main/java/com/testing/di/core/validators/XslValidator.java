/**
 * 
 */
package com.testing.di.core.validators;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * @author linoj.wilson
 *
 */
public class XslValidator {

	
	public static String transform(byte[] xml, String xslPath) throws Throwable {
		
		String transformedXml = null;
		
		if (xslPath != null) {

			Source source = new StreamSource(new ByteArrayInputStream(xml));
			InputStream xslSheet = Thread.currentThread().getContextClassLoader().getResourceAsStream(xslPath);
			Source xsl = new StreamSource(xslSheet);

			StringWriter destination = new StringWriter();
			Result result = new StreamResult(destination);

			TransformerFactory transFactory = TransformerFactory.newInstance();

			// 'xsl:import' to load other xsls from class path
			transFactory.setURIResolver(new ClasspathResourceURIResolver());
			Transformer transformer = null;
			
			try {
				transformer = transFactory.newTransformer(xsl);
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.transform(source, result);
				transformedXml = destination.toString();
				
			}

			finally

			{
				// do nothing.
			}

		}

		return transformedXml;
	}

	private static class ClasspathResourceURIResolver implements URIResolver {
		@Override
		public Source resolve(String href, String base) throws TransformerException {

			InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(href);
			return new StreamSource(in);
		}
	}

}
