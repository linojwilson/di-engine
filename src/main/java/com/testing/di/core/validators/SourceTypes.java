/**
 * 
 */
package com.testing.di.core.validators;

/**
 * @author linoj.wilson
 *
 */
public enum SourceTypes {
	
	SCHEMA_SOURCE("Schema"), VALIDATOR_SOURCE("Business Validation"), 
	ACCORD_SOURCE("Accord Specifications");
	
	private final String sourcType;

	/**
	 * @param text
	 */
	private SourceTypes(final String sourcType) {
		this.sourcType = sourcType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return sourcType;
	}

}
