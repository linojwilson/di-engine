/**
 * 
 */
package com.testing.di.core.validators;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.testing.di.core.processors.Message;


/**
 * @author linoj.wilson
 *
 */
public class DefaultXmlValidator implements XmlValidator {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultXmlValidator.class);

	private Schema schema;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.testing.bas.core.readers.XmlValidator#validate(java.lang.String)
	 */
	@Override
	public ValidationErrors validate(String xml, Message msg) throws Throwable {
		XsdErrorHandler handler = new XsdErrorHandler(msg);
		try

		{
			Validator validator = schema.newValidator();
			validator.setErrorHandler(handler);
		    validator.validate(new StreamSource(new ByteArrayInputStream(xml.getBytes())));
		}

		catch (Throwable e)

		{
			LOGGER.error(e.getMessage(), e);
			throw e;
		}

		return handler.getErrors();
	}

	@Override
	public void init(String xsd) throws Throwable 
	
	{
		if ( xsd == null) {
			//validate xml without XSD? throw up
			throw new IllegalStateException(xsd);
		}
		
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants
				.W3C_XML_SCHEMA_NS_URI);
		// associate the schema factory with the resource resolver, 
		// which is responsible for resolving the imported XSD's
	    factory.setResourceResolver(new ResourceResolver());

		InputStream is = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(xsd);
		schema = factory.newSchema(new StreamSource(is));

	}

	private static class XsdErrorHandler implements ErrorHandler {

		private ValidationErrors failures;
		private Message msg;

		public XsdErrorHandler(Message msg) {
			this.msg = msg;

			failures = new ValidationErrors();
			failures.setMsgId(msg.getMsgId());
			failures.setPeriodId(msg.getPeriodId());
			failures.setMsgType(msg.getMsgType());

		}

		public void fatalError(SAXParseException e) throws SAXException {
			// we have fatal exception. No-go. Throw out!
			LOGGER.info("Error:" + e.getMessage());
			throw e;
		}

		public void error(SAXParseException e) throws SAXException {
			// we will process the error and parse
			// message.
			String s = e.getMessage();
			// This is a case of data type being invalid.
			if ( s.contains("cvc-type.3.1.3:")) {
				LOGGER.error(e.getMessage());
				failures.incr();
				ValidationError se = new ValidationError();
				se.setMsgId(msg.getMsgId());
				se.setGuid(msg.getKey());
				se.setMsgType(msg.getMsgType());
				se.setType(ValidationTypes.DATA_TYPE.toString());
				se.setCode(ValidationTypes.DATA_ERROR.toString());
				se.setDesc(ValidationTypes.DATA_ERROR_DESC.toString());
				se.setMsg(e.getMessage());
				se.setSource(SourceTypes.SCHEMA_SOURCE.toString());
				failures.addValidationError(se);
			}
			

		}

		public void warning(SAXParseException e) throws SAXException {
			//ignore.
		}

		public ValidationErrors getErrors() {
			return failures;
		}

	}

}
