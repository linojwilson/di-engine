/**
 * 
 */
package com.testing.di.core.validators;

import com.testing.di.core.processors.Message;

/**
 * @author linoj.wilson
 *
 */
public interface Validator {
	
	public void validate(Message message);

}
