package com.testing.di.core.validators;

public enum ValidationTypes {

	DATA_TYPE("DATA_TYPE"), DATA_ERROR("DATA_ERROR"), BUSINESS_TYPE("BUSINESS_TYPE"), 
		DATA_ERROR_DESC("Invalid Data Type"), PROCESSING_ERROR("ERROR"), 
			SUCCESS("SUCCESS"), BUSINESS_ERROR_DESC("Business validation Failed");

	private final String errorType;

	/**
	 * @param text
	 */
	private ValidationTypes(final String errorType) {
		this.errorType = errorType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return errorType;
	}
}
