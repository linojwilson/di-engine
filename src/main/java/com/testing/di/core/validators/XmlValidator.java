/**
 * 
 */
package com.testing.di.core.validators;

import com.testing.di.core.processors.Message;

/**
 * @author linoj.wilson
 *
 */
public interface XmlValidator {
	
	public ValidationErrors validate(String xml, Message msg) throws Throwable;
	public void init(String xsd) throws Throwable;

}
