package com.testing.di.core.validators;

import java.util.ArrayList;
import java.util.List;

/**
 * @author linoj.wilson
 *
 */
public class ValidationErrors 

{
	
	private String msgId;
	private Long periodId;
	private String msgType;
	private String system;
	private int count;
	private List<ValidationError> validationErrors;
	
	public ValidationErrors() 
	
	{
		validationErrors = new ArrayList<ValidationError>();
		
	}
	
	/**
	 * Add validation errors.
	 * 
	 * @param error
	 */
	public void addValidationError(ValidationError error) {
		validationErrors.add(error);
	}

	/**
	 * @return the msgId
	 */
	public String getMsgId() {
		return msgId;
	}

	/**
	 * @param msgId the msgId to set
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	
	public void incr() {
		count++;
	}

	public int getCount() {
		return count;
	}

	/**
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * @param system the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}

	/**
	 * @return the syntaxErrors
	 */
	public List<ValidationError> getValidationErrors() {
		return validationErrors;
	}
	
	


}
