/**
 * 
 */
package com.testing.di.core.validators.custom;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.testing.di.core.processors.Message;
import com.testing.di.core.validators.SourceTypes;
import com.testing.di.core.validators.ValidationError;
import com.testing.di.core.validators.ValidationTypes;
import com.testing.di.core.validators.Validator;
import com.testing.di.util.ConnectionManager;

/**
 * @author linoj.wilson
 *
 */
public class CountryValidator implements Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CountryValidator.class);
	
	private String sql = "SELECT segment FROM COUNTRY_MASTER WHERE COUNTRY_CODE = ?";
	private String ccCode;

	/* (non-Javadoc)
	 * @see com.testing.bas.core.validators.Validator#validate()
	 */
	@Override
	public void validate(Message msg) {
		// lookup country code.
		LOGGER.info("Looking up country code");
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try
		
		{
			conn = ConnectionManager.getInstance()
					.getConnection();
			
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, ccCode);
			rs = stmt.executeQuery();
			boolean valid = false;
			String segment = null;
			String desc = ccCode + "is invalid";
			if ( rs.next()) {
				segment = rs.getString(1).trim();
				if (segment.
						equalsIgnoreCase("European Union (EU)")){
					valid = true;
				}
				
				else
				{
					valid = false;
					desc = ccCode + " is " + segment;
				}
			}
			
			if ( !valid) 
			{
				// we will add validation errors and
				// move on!
				ValidationError v = new ValidationError();
				v.setMsgId(msg.getMsgId());
				v.setGuid(msg.getKey());
				v.setMsgType(msg.getMsgType());
				v.setType(ValidationTypes.BUSINESS_TYPE.toString());
				v.setCode(String.valueOf("COUNTRY_LOOKUP"));
				v.setDesc("Country lookup failed");
				v.setMsg(desc);
				v.setSource(SourceTypes.VALIDATOR_SOURCE.toString());
				msg.addValidationError(v);
				
				
			}
			
		}
		
		catch ( Throwable t)
		
		{
			LOGGER.error(t.getMessage(),t);
		}
		
		finally
		
		{
			if ( conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					//ignored.
				}
			}
		}

	}

	/**
	 * @return the ccCode
	 */
	public String getCcCode() {
		return ccCode;
	}

	/**
	 * @param ccCode the ccCode to set
	 */
	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

}
